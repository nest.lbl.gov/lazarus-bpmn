package gov.lbl.nest.lazarus.bpmn;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigInteger;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;
import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.xpath.XPathFunctionResolver;
import javax.xml.xpath.XPathVariableResolver;

import gov.lbl.nest.common.configure.InitializingException;
import gov.lbl.nest.common.suspension.Suspendable;
import gov.lbl.nest.common.suspension.SuspendableImpl;
import gov.lbl.nest.common.tasks.Consumer;
import gov.lbl.nest.common.tasks.Supplier;
import gov.lbl.nest.lazarus.data.DataScope;
import gov.lbl.nest.lazarus.execution.ProcessDefinition;
import gov.lbl.nest.lazarus.management.ProcessManager;
import gov.lbl.nest.lazarus.management.ProcessManagerImpl;
import gov.lbl.nest.lazarus.structure.Activity;
import gov.lbl.nest.lazarus.structure.DataInput;
import gov.lbl.nest.lazarus.structure.DataInputAssociation;
import gov.lbl.nest.lazarus.structure.DataInputSource;
import gov.lbl.nest.lazarus.structure.DataInputTarget;
import gov.lbl.nest.lazarus.structure.DataObject;
import gov.lbl.nest.lazarus.structure.DataOutput;
import gov.lbl.nest.lazarus.structure.DataOutputAssociation;
import gov.lbl.nest.lazarus.structure.DataOutputSource;
import gov.lbl.nest.lazarus.structure.DataOutputTarget;
import gov.lbl.nest.lazarus.structure.EndEvent;
import gov.lbl.nest.lazarus.structure.ExclusiveGateway;
import gov.lbl.nest.lazarus.structure.Expression;
import gov.lbl.nest.lazarus.structure.FlowElement;
import gov.lbl.nest.lazarus.structure.FlowNode;
import gov.lbl.nest.lazarus.structure.FlowNodeTask;
import gov.lbl.nest.lazarus.structure.Gateway.GatewayDirection;
import gov.lbl.nest.lazarus.structure.InputDataItem;
import gov.lbl.nest.lazarus.structure.InputOutputSpecification;
import gov.lbl.nest.lazarus.structure.InputSet;
import gov.lbl.nest.lazarus.structure.Interface;
import gov.lbl.nest.lazarus.structure.ItemDefinition;
import gov.lbl.nest.lazarus.structure.LoopCharacteristics;
import gov.lbl.nest.lazarus.structure.Message;
import gov.lbl.nest.lazarus.structure.MultiInstanceLoopCharacteristics;
import gov.lbl.nest.lazarus.structure.Operation;
import gov.lbl.nest.lazarus.structure.OutputSet;
import gov.lbl.nest.lazarus.structure.ParallelGateway;
import gov.lbl.nest.lazarus.structure.Process;
import gov.lbl.nest.lazarus.structure.SequenceFlow;
import gov.lbl.nest.lazarus.structure.ServiceTask;
import gov.lbl.nest.lazarus.structure.StartEvent;
import gov.lbl.nest.lazarus.structure.StructureFactory;
import gov.lbl.nest.lazarus.xml.bind.ObjectFactory;
import gov.lbl.nest.lazarus.xml.bind.TActivity;
import gov.lbl.nest.lazarus.xml.bind.TBaseElement;
import gov.lbl.nest.lazarus.xml.bind.TDataInput;
import gov.lbl.nest.lazarus.xml.bind.TDataInputAssociation;
import gov.lbl.nest.lazarus.xml.bind.TDataObject;
import gov.lbl.nest.lazarus.xml.bind.TDataOutput;
import gov.lbl.nest.lazarus.xml.bind.TDataOutputAssociation;
import gov.lbl.nest.lazarus.xml.bind.TDefinitions;
import gov.lbl.nest.lazarus.xml.bind.TEndEvent;
import gov.lbl.nest.lazarus.xml.bind.TExclusiveGateway;
import gov.lbl.nest.lazarus.xml.bind.TExpression;
import gov.lbl.nest.lazarus.xml.bind.TFlowElement;
import gov.lbl.nest.lazarus.xml.bind.TFlowNode;
import gov.lbl.nest.lazarus.xml.bind.TFormalExpression;
import gov.lbl.nest.lazarus.xml.bind.TInputOutputSpecification;
import gov.lbl.nest.lazarus.xml.bind.TInputSet;
import gov.lbl.nest.lazarus.xml.bind.TInterface;
import gov.lbl.nest.lazarus.xml.bind.TItemDefinition;
import gov.lbl.nest.lazarus.xml.bind.TLoopCharacteristics;
import gov.lbl.nest.lazarus.xml.bind.TMessage;
import gov.lbl.nest.lazarus.xml.bind.TMultiInstanceLoopCharacteristics;
import gov.lbl.nest.lazarus.xml.bind.TOperation;
import gov.lbl.nest.lazarus.xml.bind.TOutputSet;
import gov.lbl.nest.lazarus.xml.bind.TParallelGateway;
import gov.lbl.nest.lazarus.xml.bind.TProcess;
import gov.lbl.nest.lazarus.xml.bind.TRootElement;
import gov.lbl.nest.lazarus.xml.bind.TSequenceFlow;
import gov.lbl.nest.lazarus.xml.bind.TServiceTask;
import gov.lbl.nest.lazarus.xml.bind.TStartEvent;

/**
 * This class is used to read and write an XML BPMN {@link TDefinitions}
 * instance to create or store one or more {@link Process} instances.
 *
 * @author patton
 */
public class XMLDefinitions {

    /**
     * The namespace for java classes.
     */
    public static final String JAVATYPE_NAMESPACE = "http://www.java.com/javaTypes";

    /**
     * The namespace for XML Schema classes.
     */
    public static final String XML_SCHEMA_NAMESPACE = "http://www.w3.org/2001/XMLSchema";

    /**
     * The {@link QName} instance use to look up the language of a
     * {@link TExpression} instance.
     */
//    private static final QName LANGUAGE_QNAME = new QName(BpmnNamespaceContext.BPMN_NS,
//                                                          "language",
//                                                          BpmnNamespaceContext.BPMN_PREFIX);

    /**
     * The {@link StructureFactory} instance to use to build the workflows.
     */
    private static final StructureFactory factory = StructureFactory.getStructureFactory();

    /**
     * Returns the contents of a {@link TExpression} instance in the form of a
     * {@link String} instance
     *
     * @param expression
     *            the {@link TExpression} instance cwhose context should be
     *            returned.
     *
     * @return the contents of a {@link TExpression} instance in the form of a
     *         {@link String} instance
     */
    private static String getExpressionContents(final TExpression expression) {
        final List<Serializable> content = expression.getContent();
        final StringBuffer sb = new StringBuffer();
        for (Serializable item : content) {
            sb.append((String) item);
        }
        final String result = sb.toString();
        return result.replace("::",
                              ":");
    }

    /**
     * Tests out the parsing of a BPMN XML file from the command line.
     *
     * @param args
     *            the command line arguments.
     *
     * @throws NoSuchElementException
     *             when the BPMN declares an element that does not exist.
     * @throws NoSuchMethodException
     *             when no external method can be mapped to one or more of the
     *             {@link Operation} instance being created.
     * @throws ClassNotFoundException
     *             when the BPMN declares an Class that does not exist.
     * @throws IOException
     *             when an IO problem occurs
     * @throws JAXBException
     *             when the BPMN can not be parsed.
     */
    public static void main(String args[]) throws ClassNotFoundException,
                                           JAXBException,
                                           IOException,
                                           NoSuchElementException,
                                           NoSuchMethodException {
        if (1 > args.length) {
            throw new IllegalArgumentException("Filename must be specified");
        }
        final InputStream inputStream = new FileInputStream(args[0]);
        final XMLDefinitions xMLDefinitions = new XMLDefinitions();
        xMLDefinitions.parseDefinitions(inputStream);
    }

    /**
     * <code>true</code> if the JAVATYPE_NAMESPACE is required for ItemDefinition.
     */
    private final boolean requireJavaTypeNamespace;

    /**
     * Creates an instance of this class.
     */
    public XMLDefinitions() {
        this(false);
    }

    /**
     * Creates an instance of this class.
     *
     * @param requireJavaTypeNamespace
     *            <code>true</code> if the JAVATYPE_NAMESPACE is required for
     *            ItemDefinition.
     */
    XMLDefinitions(boolean requireJavaTypeNamespace) {
        this(null,
             null,
             null,
             requireJavaTypeNamespace);
    }

    /**
     * Creates an instance of this class.
     *
     * @param namespaceContext
     *            the {@link NamespaceContext} instance to use if this object does
     *            not provide a suitable one.
     * @param functionResolver
     *            the {@link XPathVariableResolver} instance to use if this object
     *            does not provide a suitable one.
     * @param variableResolver
     *            the {@link XPathVariableResolver} instance to use if this object
     *            does not provide a suitable one.
     * @param requireJavaTypeNamespace
     *            <code>true</code> if the JAVATYPE_NAMESPACE is required for
     *            ItemDefinition.
     */
    public XMLDefinitions(NamespaceContext namespaceContext,
                          XPathFunctionResolver functionResolver,
                          XPathVariableResolver variableResolver,
                          boolean requireJavaTypeNamespace) {
        this.requireJavaTypeNamespace = requireJavaTypeNamespace;
        factory.setFallbackNamespaceContext(namespaceContext);
        factory.setFallbackXPathFunctionResolver(functionResolver);
        factory.setFallbackXPathVariableResolver(variableResolver);
    }

    /**
     * Creates a local {@link Activity} instance from the supplied {@link TActivity}
     * instance.
     *
     * @param activity
     *            the {@link TActivity} instance from which to create the
     *            {@link Activity} instance.
     * @param dataObjects
     *            the collection of {@link DataObject} instances in the current,
     *            index by identity. {@link DataScope} instance.
     * @param operations
     *            the collection of {@link Operation} instance in the BPMN diagram,
     *            index by identity.
     * @param itemDefinitions
     *            the collection of {@link ItemDefinition} instance in the BPMN
     *            diagram, index by identity.
     * @param supplier
     *            the {@link Supplier} instance to which {@link FlowNodeTask}
     *            instance in an executing workflow will be added.
     * @param suspenable
     *            the {@link Suspendable} instance used by this object to know
     *            whether it is active or not.
     * 
     * @return the created {@link Activity} instance.
     * 
     * @throws ClassNotFoundException
     */
    private Activity createActivity(TActivity activity,
                                    Map<String, ? extends DataObject> dataObjects,
                                    Map<String, ? extends Operation> operations,
                                    Map<String, ? extends ItemDefinition> itemDefinitions,
                                    Supplier<FlowNodeTask> supplier,
                                    Suspendable suspendable) throws ClassNotFoundException {
        final InputOutputSpecification ioSpecification;
        if (null == activity.getIoSpecification()) {
            ioSpecification = null;
        } else {
            ioSpecification = createIoSpecification(activity.getIoSpecification(),
                                                    itemDefinitions);
        }
        final List<DataInput> dataInputs = new ArrayList<>(ioSpecification.getDataInputs());
        final List<DataOutput> dataOutputs = new ArrayList<>(ioSpecification.getDataOutputs());

        final JAXBElement<? extends TLoopCharacteristics> loopElement = activity.getLoopCharacteristics();
        final LoopCharacteristics loopCharacteristics;
        ;
        if (null == loopElement) {
            loopCharacteristics = null;
        } else {
            final TLoopCharacteristics characteristics = loopElement.getValue();
            if (characteristics instanceof TMultiInstanceLoopCharacteristics) {
                final MultiInstanceLoopCharacteristics multiLoopCharacteristics = createMultiInstanceLoopCharacteristics((TMultiInstanceLoopCharacteristics) characteristics,
                                                                                                                         dataObjects,
                                                                                                                         ioSpecification,
                                                                                                                         itemDefinitions);
                final DataInput inputItem = multiLoopCharacteristics.getInputDataItem();
                if (null != inputItem) {
                    dataInputs.add(inputItem);
                }
                final DataOutput outputItem = multiLoopCharacteristics.getOutputDataItem();
                if (null != outputItem) {
                    dataOutputs.add(outputItem);
                }

                loopCharacteristics = multiLoopCharacteristics;
            } else {
                throw new NoSuchElementException("This application does not know how to handle a " + (characteristics.getClass()).getName()
                                                 + " element");
            }
        }

        final List<DataInputAssociation<? extends DataInputSource, ? extends DataInputTarget>> inputAssocs;
        if (null == activity.getDataInputAssociation()) {
            inputAssocs = null;
        } else {
            inputAssocs = createDataInputAssociations(activity,
                                                      dataObjects,
                                                      dataInputs);
        }

        final List<DataOutputAssociation<? extends DataOutputSource, ? extends DataOutputTarget>> outputAssocs;
        if (null == activity.getDataOutputAssociation()) {
            outputAssocs = null;
        } else {

            outputAssocs = createDataOutputAssociations(activity,
                                                        dataObjects,
                                                        dataOutputs);
        }

        if (activity instanceof TServiceTask) {
            return createServiceTask((TServiceTask) activity,
                                     operations,
                                     ioSpecification,
                                     inputAssocs,
                                     outputAssocs,
                                     loopCharacteristics,
                                     supplier,
                                     suspendable);
        }
        throw new NoSuchElementException("This application does not know how to handle a " + (activity.getClass()).getName()
                                         + " element");
    }

    private DataInput createDataInput(TDataInput dataInput,
                                      final ItemDefinition itemDefinition) throws ClassNotFoundException {
        return factory.createDataInput(dataInput.getName(),
                                       dataInput.getId(),
                                       itemDefinition,
                                       dataInput.isIsCollection());
    }

    private List<DataInputAssociation<? extends DataInputSource, ? extends DataInputTarget>> createDataInputAssociations(TActivity activity,
                                                                                                                         Map<String, ? extends DataObject> dataObjects,
                                                                                                                         List<? extends DataInput> dataInputs) {
        Map<String, DataInputSource> inputSources = new HashMap<>();
        inputSources.putAll(dataObjects);
        for (DataInput dataInput : dataInputs) {
            inputSources.put(dataInput.getIdentity(),
                             dataInput);
        }

        Map<String, DataInputTarget> inputTargets = new HashMap<>();
        for (DataInput dataInput : dataInputs) {
            inputTargets.put(dataInput.getIdentity(),
                             dataInput);
        }

        final List<DataInputAssociation<? extends DataInputSource, ? extends DataInputTarget>> inputAssocs = createInputAssocs(activity.getDataInputAssociation(),
                                                                                                                               inputSources,
                                                                                                                               inputTargets);
        return inputAssocs;
    }

    private DataObject createDataObject(TDataObject dataObject,
                                        Map<String, ItemDefinition> itemDefinitions) throws ClassNotFoundException {
        final QName itemSubjectRef = dataObject.getItemSubjectRef();
        return factory.createDataObject(dataObject.getName(),
                                        dataObject.getId(),
                                        itemDefinitions.get(itemSubjectRef.getLocalPart()),
                                        dataObject.isIsCollection());
    }

    private DataOutput createDataOutput(TDataOutput dataOutput,
                                        Map<String, ? extends ItemDefinition> itemDefinitions) throws ClassNotFoundException {
        final QName itemSubjectRef = dataOutput.getItemSubjectRef();
        final ItemDefinition itemSubject;
        if (null == itemSubjectRef) {
            itemSubject = null;
        } else {
            itemSubject = itemDefinitions.get(itemSubjectRef.getLocalPart());
        }
        return factory.createDataOutput(dataOutput.getName(),
                                        dataOutput.getId(),
                                        itemSubject,
                                        dataOutput.isIsCollection());
    }

    private List<DataOutputAssociation<? extends DataOutputSource, ? extends DataOutputTarget>> createDataOutputAssociations(TActivity activity,
                                                                                                                             Map<String, ? extends DataObject> dataObjects,
                                                                                                                             List<? extends DataOutput> dataOutputs) {
        Map<String, DataOutputSource> outputSources = new HashMap<>();
        for (DataOutput dataOutput : dataOutputs) {
            outputSources.put(dataOutput.getIdentity(),
                              dataOutput);
        }

        Map<String, DataOutputTarget> outputTargets = new HashMap<>();
        outputTargets.putAll(dataObjects);
        for (DataOutput dataOutput : dataOutputs) {
            outputTargets.put(dataOutput.getIdentity(),
                              dataOutput);
        }

        final List<DataOutputAssociation<? extends DataOutputSource, ? extends DataOutputTarget>> outputAssocs = createOutputAssocs(activity.getDataOutputAssociation(),
                                                                                                                                    outputSources,
                                                                                                                                    outputTargets);
        return outputAssocs;
    }

    /**
     * Creates a local {@link EndEvent} instance from the supplied {@link TEndEvent}
     * instance.
     *
     * @param gateway
     *            the {@link TEndEvent} instance from which to create the
     *            {@link EndEvent} instance.
     *
     * @return the created {@link EndEvent} instance.
     */
    private EndEvent createEndEvent(TEndEvent event) {
        final EndEvent created = factory.createEndEvent(event.getName(),
                                                        event.getId());
        return created;
    }

    /**
     * Creates a local {@link ExclusiveGateway} instance from the supplied
     * {@link TExclusiveGateway} instance.
     *
     * @param gateway
     *            the {@link TExclusiveGateway} instance from which to create the
     *            {@link ExclusiveGateway} instance.
     *
     * @return the created {@link ExclusiveGateway} instance.
     */
    private ExclusiveGateway createExclusiveGateway(TExclusiveGateway gateway) {
        final GatewayDirection direction = GatewayDirection.valueOf(((gateway).getGatewayDirection()).toString());
        final ExclusiveGateway created = factory.createExclusiveGateway(gateway.getName(),
                                                                        gateway.getId(),
                                                                        direction);
        return created;
    }

    /**
     * Creates a local {@link Expression} instance from the supplied
     * {@link TExpression} instance.
     *
     * @param task
     *            the {@link TExpression} instance from which to create the
     *            {@link Expression} instance.
     * @param clazz
     *            the {@link Class} instance of the return type of the returned
     *            {@link Expression} instance.
     *
     * @return the created {@link ServiceTask} instance.
     */
    private <T> Expression<T> createExpression(TExpression expression,
                                               Class<T> clazz) {
        String contents = getExpressionContents(expression);
        if (expression instanceof TFormalExpression) {
            final String language = ((TFormalExpression) expression).getLanguage();
            return factory.createExpression(expression.getId(),
                                            contents,
                                            language,
                                            clazz);
        }
        throw new UnsupportedOperationException("Only Formal Expressions are currenly suported");
    }

    private DataInputAssociation<? extends DataInputSource, ? extends DataInputTarget> createInputAssoc(TDataInputAssociation inputAssoc,
                                                                                                        Map<String, ? extends DataInputSource> inputSources,
                                                                                                        Map<String, ? extends DataInputTarget> inputTargets) {
        final List<JAXBElement<Object>> sourceElements = inputAssoc.getSourceRef();
        final List<DataInputSource> sources = new ArrayList<>();
        for (JAXBElement<Object> jaxbElement : sourceElements) {
            final TBaseElement sourceElement = (TBaseElement) jaxbElement.getValue();
            sources.add(inputSources.get(sourceElement.getId()));
        }

        final TBaseElement targetElement = (TBaseElement) inputAssoc.getTargetRef();
        DataInputTarget target = inputTargets.get(targetElement.getId());
        final DataInputAssociation<? extends DataInputSource, ? extends DataInputTarget> created = factory.createDataInputAssociation(inputAssoc.getId(),
                                                                                                                                      sources,
                                                                                                                                      target);
        return created;
    }

    private List<DataInputAssociation<? extends DataInputSource, ? extends DataInputTarget>> createInputAssocs(List<TDataInputAssociation> inputAssocs,
                                                                                                               Map<String, ? extends DataInputSource> inputSources,
                                                                                                               Map<String, ? extends DataInputTarget> inputTargets) {
        final List<DataInputAssociation<? extends DataInputSource, ? extends DataInputTarget>> results = new ArrayList<>();
        for (TDataInputAssociation inputAssoc : inputAssocs) {
            results.add(createInputAssoc(inputAssoc,
                                         inputSources,
                                         inputTargets));
        }
        return results;
    }

    private InputDataItem createInputDataItem(TDataInput inputDataItem,
                                              DataInputSource loopDataInputRef,
                                              Map<String, ? extends ItemDefinition> itemDefinitions) throws ClassNotFoundException {
        final QName itemSubjectRef = inputDataItem.getItemSubjectRef();
        final ItemDefinition itemSubject;
        if (null == itemSubjectRef) {
            itemSubject = null;
        } else {
            itemSubject = itemDefinitions.get(itemSubjectRef.getLocalPart());
        }
        return factory.createInputDataItem(inputDataItem.getName(),
                                           inputDataItem.getId(),
                                           loopDataInputRef,
                                           itemSubject,
                                           inputDataItem.isIsCollection());
    }

    /**
     * Creates a local {@link Interface} instance from the supplied
     * {@link TInterface} instance.
     *
     * @param iface
     *            the {@link TInterface} instance from which to create the
     *            {@link Interface} instance.
     * @param messages
     *            the collection of known {@link Message} instances, index by their
     *            identity.
     *
     * @return the created {@link Interface} instance.
     *
     * @throws ClassNotFoundException
     *             when the requested {@link Class} can not be found.
     * @throws NoSuchMethodException
     *             when no external method can be mapped to one or more of the
     *             {@link Operation} instance being created.
     */
    private Interface createInterface(TInterface iface,
                                      Map<String, Message> messages) throws ClassNotFoundException,
                                                                     NoSuchMethodException {
        final List<TOperation> ops = iface.getOperation();
        final List<Operation> operations = new ArrayList<>();
        for (TOperation op : ops) {
            operations.add(createOperation(op,
                                           messages));
        }
        final QName implementationRef = iface.getImplementationRef();
        return factory.createInterface(iface.getName(),
                                       iface.getId(),
                                       operations,
                                       implementationRef.getLocalPart());
    }

    private InputOutputSpecification createIoSpecification(TInputOutputSpecification ioSpecification,
                                                           Map<String, ? extends ItemDefinition> itemDefinitions) throws ClassNotFoundException {
        final List<TDataInput> inputs = ioSpecification.getDataInput();
        Map<String, DataInput> dataInputsById = new HashMap<>();
        for (TDataInput dataInput : inputs) {
            final QName itemSubjectRef = dataInput.getItemSubjectRef();
            final ItemDefinition itemDefinition = itemDefinitions.get(itemSubjectRef.getLocalPart());
            final DataInput created = createDataInput(dataInput,
                                                      itemDefinition);
            dataInputsById.put(created.getIdentity(),
                               created);
        }

        final List<TDataOutput> outputs = ioSpecification.getDataOutput();
        Map<String, DataOutput> dataOutputsById = new HashMap<>();
        for (TDataOutput dataOutput : outputs) {
            final DataOutput created = createDataOutput(dataOutput,
                                                        itemDefinitions);
            dataOutputsById.put(created.getIdentity(),
                                created);
        }

        final List<TInputSet> inSets = ioSpecification.getInputSet();
        List<InputSet> inputSets = new ArrayList<>();
        for (TInputSet inSet : inSets) {
            final List<JAXBElement<Object>> inputElements = inSet.getDataInputRefs();
            final List<DataInput> inputsInSet = new ArrayList<>();
            for (JAXBElement<Object> jaxbElement : inputElements) {
                final TBaseElement baseElement = (TBaseElement) jaxbElement.getValue();
                inputsInSet.add(dataInputsById.get(baseElement.getId()));
            }
            inputSets.add(factory.createInputSet(inSet.getName(),
                                                 inSet.getId(),
                                                 inputsInSet));
        }

        final List<TOutputSet> outSets = ioSpecification.getOutputSet();
        List<OutputSet> outputSets = new ArrayList<>();
        for (TOutputSet outSet : outSets) {
            final List<JAXBElement<Object>> outputElements = outSet.getDataOutputRefs();
            final List<DataOutput> outputsInSet = new ArrayList<>();
            for (JAXBElement<Object> jaxbElement : outputElements) {
                final TBaseElement baseElement = (TBaseElement) jaxbElement.getValue();
                outputsInSet.add(dataOutputsById.get(baseElement.getId()));
            }
            outputSets.add(factory.createOutputSet(outSet.getName(),
                                                   outSet.getId(),
                                                   outputsInSet));
        }

        final InputOutputSpecification created = factory.createInputOutputSpecification(ioSpecification.getId(),
                                                                                        dataInputsById.values(),
                                                                                        dataOutputsById.values(),
                                                                                        inputSets,
                                                                                        outputSets);
        return created;
    }

    /**
     * Creates a local {@link ItemDefinition} instance from the supplied
     * {@link TItemDefinition} instance.
     *
     * @param itemDefinition
     *            the {@link TItemDefinition} instance from which to create the
     *            {@link ItemDefinition} instance.
     *
     * @return the created {@link ItemDefinition} instance.
     *
     * @throws ClassNotFoundException
     *             when the requested {@link Class} can not be found.
     * @throws UnsupportedOperationException
     *             if the requested structure is not supported. Currently only the
     *             javaType namespace is supported.
     */
    private ItemDefinition createItemDefinition(TItemDefinition itemDefinition) throws ClassNotFoundException {
        final QName structureRef = itemDefinition.getStructureRef();
        if (XML_SCHEMA_NAMESPACE.equals(structureRef.getNamespaceURI())) {
            if ("boolean".equals(structureRef.getLocalPart())) {
                return factory.createItemDefinition(itemDefinition.getId(),
                                                    (Boolean.class).getName(),
                                                    itemDefinition.isIsCollection());
            }
            if ("byte".equals(structureRef.getLocalPart())) {
                return factory.createItemDefinition(itemDefinition.getId(),
                                                    (Byte.class).getName(),
                                                    itemDefinition.isIsCollection());
            }
            if ("float".equals(structureRef.getLocalPart())) {
                return factory.createItemDefinition(itemDefinition.getId(),
                                                    (Float.class).getName(),
                                                    itemDefinition.isIsCollection());
            }
            if ("double".equals(structureRef.getLocalPart())) {
                return factory.createItemDefinition(itemDefinition.getId(),
                                                    (Double.class).getName(),
                                                    itemDefinition.isIsCollection());
            }
            if ("int".equals(structureRef.getLocalPart())) {
                return factory.createItemDefinition(itemDefinition.getId(),
                                                    (Integer.class).getName(),
                                                    itemDefinition.isIsCollection());
            }
            if ("integer".equals(structureRef.getLocalPart())) {
                return factory.createItemDefinition(itemDefinition.getId(),
                                                    (BigInteger.class).getName(),
                                                    itemDefinition.isIsCollection());
            }
            if ("long".equals(structureRef.getLocalPart())) {
                return factory.createItemDefinition(itemDefinition.getId(),
                                                    (Long.class).getName(),
                                                    itemDefinition.isIsCollection());
            }
            if ("short".equals(structureRef.getLocalPart())) {
                return factory.createItemDefinition(itemDefinition.getId(),
                                                    (Short.class).getName(),
                                                    itemDefinition.isIsCollection());
            }
            if ("string".equals(structureRef.getLocalPart())) {
                return factory.createItemDefinition(itemDefinition.getId(),
                                                    (String.class).getName(),
                                                    itemDefinition.isIsCollection());
            }
        }
        if (requireJavaTypeNamespace && !JAVATYPE_NAMESPACE.equals(structureRef.getNamespaceURI())) {
            throw new UnsupportedOperationException("Only structures in the \"" + JAVATYPE_NAMESPACE
                                                    + "\" namespace are supported");
        }
        return factory.createItemDefinition(itemDefinition.getId(),
                                            structureRef.getLocalPart(),
                                            itemDefinition.isIsCollection());
    }

    /**
     * Creates a local {@link Message} instance from the supplied {@link TMessage}
     * instance.
     *
     * @param itemDefinition
     *            the {@link TMessage} instance from which to create the
     *            {@link Message} instance.
     *
     * @return the created {@link Message} instance.
     *
     * @throws ClassNotFoundException
     *             when the requested {@link Class} can not be found.
     * @throws UnsupportedOperationException
     *             if the requested structure is not supported. Currently only the
     *             javaType namespace is supported.
     */
    private Message createMessage(TMessage message,
                                  Map<String, ItemDefinition> itemDefinitions) throws ClassNotFoundException {
        final QName itemRef = message.getItemRef();
        final ItemDefinition item = itemDefinitions.get(itemRef.getLocalPart());
        return factory.createMessage(message.getName(),
                                     message.getId(),
                                     item);
    }

    /**
     * Creates a local {@link MultiInstanceLoopCharacteristics} instance from the
     * supplied {@link TMultiInstanceLoopCharacteristics} instance.
     *
     * @param gateway
     *            the {@link TMultiInstanceLoopCharacteristics} instance from which
     *            to create the {@link MultiInstanceLoopCharacteristics} instance.
     *
     * @return the created {@link LoopCharacteristics} instance.
     * 
     * @throws ClassNotFoundException
     */
    private MultiInstanceLoopCharacteristics createMultiInstanceLoopCharacteristics(TMultiInstanceLoopCharacteristics loopCharacter,
                                                                                    Map<String, ? extends DataObject> dataObjects,
                                                                                    InputOutputSpecification ioSpecification,
                                                                                    Map<String, ? extends ItemDefinition> itemDefinitions) throws ClassNotFoundException {
        final TExpression cardinality = loopCharacter.getLoopCardinality();
        final Expression<Integer> loopCardinality;
        if (null == cardinality) {
            loopCardinality = null;
        } else {
            loopCardinality = createExpression(cardinality,
                                               Integer.class);
        }

        final List<? extends DataInput> dataInputs = ioSpecification.getDataInputs();
        Map<String, DataInputTarget> inputTargets = new HashMap<>();
        for (DataInput dataInput : dataInputs) {
            inputTargets.put(dataInput.getIdentity(),
                             dataInput);
        }

        final QName dataInputRef = loopCharacter.getLoopDataInputRef();
        DataInput loopDataInputRef;
        if (null == dataInputRef) {
            loopDataInputRef = null;
        } else {
            loopDataInputRef = (DataInput) inputTargets.get(dataInputRef.getLocalPart());
        }

        final InputDataItem inputDataItem = createInputDataItem(loopCharacter.getInputDataItem(),
                                                                loopDataInputRef,
                                                                itemDefinitions);

        final List<? extends DataOutput> dataOutputs = ioSpecification.getDataOutputs();
        Map<String, DataOutputSource> outputSources = new HashMap<>();
        for (DataOutput dataOutput : dataOutputs) {
            outputSources.put(dataOutput.getIdentity(),
                              dataOutput);
        }

        final QName dataOutputRef = loopCharacter.getLoopDataOutputRef();
        final DataOutputSource loopDataOutputRef;
        final DataOutput outputDataItem;
        if (null == dataOutputRef) {
            loopDataOutputRef = null;
            outputDataItem = null;
        } else {
            loopDataOutputRef = outputSources.get(dataOutputRef.getLocalPart());
            outputDataItem = createDataOutput(loopCharacter.getOutputDataItem(),
                                              itemDefinitions);
        }

        final TExpression completion = loopCharacter.getCompletionCondition();
        Expression<Boolean> completionCondition;
        if (null == completion) {
            completionCondition = null;
        } else {
            completionCondition = createExpression(completion,
                                                   Boolean.class);
        }

        return factory.createMultiInstanceLoopCharacteristics(loopCharacter.getId(),
                                                              loopCardinality,
                                                              loopDataInputRef,
                                                              loopDataOutputRef,
                                                              inputDataItem,
                                                              outputDataItem,
                                                              completionCondition);
    }

    /**
     * Creates a local {@link Operation} instance from the supplied
     * {@link TOperation} instance.
     *
     * @param operation
     *            the {@link TOperation} instance from which to create the
     *            {@link Operation} instance.
     * @param messages
     *            the collection of known {@link Message} instances, index by their
     *            identity.
     *
     * @return the created {@link Interface} instance.
     *
     * @throws NoSuchMethodException
     *             when no external method can be mapped to the supplied
     *             information.
     */
    private Operation createOperation(TOperation operation,
                                      Map<String, Message> messages) throws NoSuchMethodException {
        final QName implementationRef = operation.getImplementationRef();
        final QName inMessage = operation.getInMessageRef();
        final Message inputType;
        if (null == inMessage) {
            inputType = null;
        } else {
            final String localPart = inMessage.getLocalPart();
            if (null == localPart) {
                inputType = null;
            } else {
                inputType = messages.get(localPart);
            }
        }

        final QName outMessage = operation.getOutMessageRef();
        final Message outputType;
        if (null == outMessage) {
            outputType = null;
        } else {
            final String localPart = outMessage.getLocalPart();
            if (null == localPart) {
                outputType = null;
            } else {
                outputType = messages.get(localPart);
            }
        }
        return factory.createOperation(operation.getName(),
                                       operation.getId(),
                                       implementationRef.getLocalPart(),
                                       inputType,
                                       outputType);
    }

    private DataOutputAssociation<? extends DataOutputSource, ? extends DataOutputTarget> createOutputAssoc(TDataOutputAssociation outputAssoc,
                                                                                                            Map<String, DataOutputSource> outputSources,
                                                                                                            Map<String, DataOutputTarget> outputTargets) {
        final List<JAXBElement<Object>> sourceElements = outputAssoc.getSourceRef();
        final List<DataOutputSource> sources = new ArrayList<>();
        for (JAXBElement<Object> jaxbElement : sourceElements) {
            final TBaseElement sourceElement = (TBaseElement) jaxbElement.getValue();
            sources.add(outputSources.get(sourceElement.getId()));
        }

        final TBaseElement targetElement = (TBaseElement) outputAssoc.getTargetRef();
        DataOutputTarget target = outputTargets.get(targetElement.getId());
        final DataOutputAssociation<? extends DataOutputSource, ? extends DataOutputTarget> created = factory.createDataOutputAssociation(outputAssoc.getId(),
                                                                                                                                          sources,
                                                                                                                                          target);
        return created;
    }

    private List<DataOutputAssociation<? extends DataOutputSource, ? extends DataOutputTarget>> createOutputAssocs(List<TDataOutputAssociation> outputAssocs,
                                                                                                                   Map<String, DataOutputSource> outputSources,
                                                                                                                   Map<String, DataOutputTarget> outputTargets) {
        final List<DataOutputAssociation<? extends DataOutputSource, ? extends DataOutputTarget>> results = new ArrayList<>();
        for (TDataOutputAssociation outputAssoc : outputAssocs) {
            results.add(createOutputAssoc(outputAssoc,
                                          outputSources,
                                          outputTargets));
        }
        return results;
    }

    /**
     * Creates a local {@link ParallelGateway} instance from the supplied
     * {@link TParallelGateway} instance.
     *
     * @param gateway
     *            the {@link TParallelGateway} instance from which to create the
     *            {@link ParallelGateway} instance.
     *
     * @return the created {@link ParallelGateway} instance.
     */
    private ParallelGateway createParallelGateway(TParallelGateway gateway) {
        final GatewayDirection direction = GatewayDirection.valueOf(((gateway).getGatewayDirection()).toString());
        final ParallelGateway created = factory.createParallelGateway(gateway.getName(),
                                                                      gateway.getId(),
                                                                      direction);
        return created;
    }

    /**
     * Creates a local {@link Process} instance from the supplied {@link TMessage}
     * instance.
     *
     * @param process
     *            the {@link TProcess} instance from which to create the
     *            {@link Process} instance.
     * @param operations
     *            the collection of {@link Operation} instance in the BPMN diagram,
     *            index by identity.
     * @param itemDefinitions
     *            the collection of {@link ItemDefinition} instance in the BPMN
     *            diagram, index by identity.
     * @param consumer
     *            the {@link Consumer} instance that will consume the
     *            {@link FlowNodeTask} instances from the supplier.
     * @param supplier
     *            the {@link Supplier} instance to which {@link FlowNodeTask}
     *            instance in an executing workflow will be added.
     * @param suspendable
     *            the {@link AtomicBoolean} that is <code>true</code> when the
     *            {@link Process} instance is active.
     * @param path
     *            the {@link Path} instance, if any, to the file containing the set
     *            of names of suspended {@link FlowNodeTask} instances.
     * 
     * @return the created {@link Process} instance.
     *
     * @throws ClassNotFoundException
     *             when the requested {@link Class} can not be found.
     * @throws InitializingException
     * @throws UnsupportedOperationException
     *             if the requested structure is not supported. Currently only the
     *             javaType namespace is supported.
     */
    private Process createProcess(TProcess process,
                                  Map<String, Operation> operations,
                                  Map<String, ItemDefinition> itemDefinitions,
                                  Consumer consumer,
                                  Supplier<FlowNodeTask> supplier,
                                  Suspendable suspendable,
                                  Path path) throws ClassNotFoundException {
        final ProcessManager processManager = new ProcessManagerImpl(consumer,
                                                                     suspendable,
                                                                     path);

        final List<JAXBElement<? extends TFlowElement>> flowElements = process.getFlowElement();

        final List<TDataObject> dataObjectDefinitions = new ArrayList<>();
        final List<TFlowNode> flowNodeDefinitions = new ArrayList<>();
        final List<TSequenceFlow> sequenceFlowDefinitions = new ArrayList<>();
        for (JAXBElement<? extends TFlowElement> jaxbElement : flowElements) {
            final TFlowElement flowElement = jaxbElement.getValue();
            if (flowElement instanceof TDataObject) {
                dataObjectDefinitions.add((TDataObject) flowElement);
            } else if (flowElement instanceof TFlowNode) {
                flowNodeDefinitions.add((TFlowNode) flowElement);
            } else if (flowElement instanceof TSequenceFlow) {
                sequenceFlowDefinitions.add((TSequenceFlow) flowElement);
            } else {
                throw new NoSuchElementException("This application does not know how to handle a " + (flowElement.getClass()).getName()
                                                 + " element");
            }
        }

        final Map<String, DataObject> dataObjects = new HashMap<>();
        for (TDataObject dataObject : dataObjectDefinitions) {
            final DataObject data = createDataObject(dataObject,
                                                     itemDefinitions);
            dataObjects.put(data.getIdentity(),
                            data);
        }

        StartEvent start = null;
        final Map<String, FlowNode> elements = new HashMap<>();
        for (TFlowNode flowNode : flowNodeDefinitions) {
            if (flowNode instanceof TStartEvent) {
                start = createStartEvent((TStartEvent) flowNode);
                elements.put(start.getIdentity(),
                             start);
            } else if (flowNode instanceof TExclusiveGateway) {
                final ExclusiveGateway created = createExclusiveGateway((TExclusiveGateway) flowNode);
                elements.put(created.getIdentity(),
                             created);
            } else if (flowNode instanceof TActivity) {
                final String name = flowNode.getName();
                boolean suspended;
                try {
                    suspended = processManager.isSuspended(name);
                    final Activity created = createActivity((TActivity) flowNode,
                                                            dataObjects,
                                                            operations,
                                                            itemDefinitions,
                                                            supplier,
                                                            new SuspendableImpl(name,
                                                                                suspended));
                    elements.put(created.getIdentity(),
                                 created);
                } catch (InitializingException e) {
                    throw new RuntimeException("How did the code get here?",
                                               e);
                }
            } else if (flowNode instanceof TParallelGateway) {
                final ParallelGateway created = createParallelGateway((TParallelGateway) flowNode);
                elements.put(created.getIdentity(),
                             created);
            } else if (flowNode instanceof TEndEvent) {
                final EndEvent created = createEndEvent((TEndEvent) flowNode);
                elements.put(created.getIdentity(),
                             created);
            } else {
                throw new NoSuchElementException("This application does not know how to handle a " + (flowNode.getClass()).getName()
                                                 + " element");
            }
        }

        final List<SequenceFlow> flows = new ArrayList<>();
        for (TSequenceFlow flowNode : sequenceFlowDefinitions) {
            flows.add(createSequenceFlow(flowNode,
                                         elements));
        }

        final List<FlowElement> allElements = new ArrayList<>(elements.values());
        allElements.addAll(flows);

        return factory.createProcess(process.getName(),
                                     process.getId(),
                                     start,
                                     allElements,
                                     dataObjects.values(), processManager);
    }

    /**
     * Creates a local {@link SequenceFlow} instance from the supplied
     * {@link TSequenceFlow} instance.
     *
     * @param itemDefinition
     *            the {@link TSequenceFlow} instance from which to create the
     *            {@link SequenceFlow} instance.
     *
     * @return the created {@link SequenceFlow} instance.
     *
     * @throws ClassNotFoundException
     *             when the requested {@link Class} can not be found.
     * @throws UnsupportedOperationException
     *             if the requested structure is not supported. Currently only the
     *             javaType namespace is supported.
     */
    private SequenceFlow createSequenceFlow(TSequenceFlow sequenceFlow,
                                            Map<String, FlowNode> flowNodes) throws ClassNotFoundException {

        TExpression expression = sequenceFlow.getConditionExpression();
        Expression<Boolean> expressionToUse;
        if (null == expression) {
            expressionToUse = null;
        } else {
            expressionToUse = createExpression(expression,
                                               Boolean.class);
        }
        TFlowNode sourceNode = (TFlowNode) (sequenceFlow.getSourceRef());
        final FlowNode source = flowNodes.get(sourceNode.getId());
        TFlowNode targetNode = (TFlowNode) (sequenceFlow.getTargetRef());
        final FlowNode target = flowNodes.get(targetNode.getId());
        return factory.createSequenceFlow(sequenceFlow.getName(),
                                          sequenceFlow.getId(),
                                          source,
                                          target,
                                          expressionToUse);
    }

    /**
     * Creates a local {@link ServiceTask} instance from the supplied
     * {@link TServiceTask} instance.
     *
     * @param task
     *            the {@link TServiceTask} instance from which to create the
     *            {@link ServiceTask} instance.
     * @param operations
     *            the collection of {@link Operation} instance in the BPMN diagram,
     *            index by identity.
     * @param ioSpecification
     *            the {@link InputOutputSpecification} instance that maps data in
     *            and out of this object.
     * @param inputAssocs
     *            the list of {@link DataInputAssociation} instances used to fill
     *            the {@link DataInput} elements of the
     *            {@link InputOutputSpecification} instance.
     * @param outputAssocs
     *            the list of {@link DataOutputAssociation} instances used to fill
     *            the {@link DataOutput} elements of the
     *            {@link InputOutputSpecification} instance.
     * @param loopCharacteristics
     *            the {@link LoopCharacteristics} instance, if any, defining who the
     *            created {@link ServiceTask} should loop.
     * @param supplier
     *            the {@link Supplier} instance to which {@link FlowNodeTask}
     *            instance in an executing workflow will be added.
     * @param suspendable
     *            the {@link AtomicBoolean} that is <code>true</code> when this
     *            object is active.
     * 
     * @return the created {@link ServiceTask} instance.
     */
    private ServiceTask createServiceTask(TServiceTask task,
                                          Map<String, ? extends Operation> operations,
                                          InputOutputSpecification ioSpecification,
                                          Collection<? extends DataInputAssociation<? extends DataInputSource, ? extends DataInputTarget>> inputAssocs,
                                          Collection<? extends DataOutputAssociation<? extends DataOutputSource, ? extends DataOutputTarget>> outputAssocs,
                                          LoopCharacteristics loopCharacteristics,
                                          Supplier<FlowNodeTask> supplier,
                                          Suspendable suspendable) {
        final Operation operation = operations.get((task.getOperationRef()).getLocalPart());
        final ServiceTask created = factory.createServiceTask(task.getName(),
                                                              task.getId(),
                                                              operation,
                                                              ioSpecification,
                                                              inputAssocs,
                                                              outputAssocs,
                                                              loopCharacteristics,
                                                              supplier,
                                                              suspendable);
        return created;
    }

    /**
     * Creates a local {@link StartEvent} instance from the supplied
     * {@link TStartEvent} instance.
     *
     * @param gateway
     *            the {@link TStartEvent} instance from which to create the
     *            {@link StartEvent} instance.
     *
     * @return the created {@link StartEvent} instance.
     */
    private StartEvent createStartEvent(TStartEvent event) {
        final StartEvent start;
        start = factory.createStartEvent(event.getName(),
                                         event.getId());
        return start;
    }

    /**
     * Parses the BPMN XML contains in the provided {@link InputStream}
     *
     * @param inputStream
     *            the {@link InputStream} containing the BPMN XML.
     * 
     * @throws ClassNotFoundException
     *             then class requested by the BPMXML can not be found.
     * @throws JAXBException
     *             when there is an issue with parse the
     * @throws IOException
     *             the there is an issue with the {@link InputStream} provided.
     * @throws NoSuchElementException
     *             when the BPMN declares an element that does not exist.
     * @throws NoSuchMethodException
     *             when no external method can be mapped to one or more of the
     *             {@link Operation} instance being created.
     *
     * @return returns a map of names and {@link Process} instances.
     */
    public Map<String, ? extends ProcessDefinition> parseDefinitions(InputStream inputStream) throws ClassNotFoundException,
                                                                                              JAXBException,
                                                                                              IOException,
                                                                                              NoSuchElementException,
                                                                                              NoSuchMethodException {
        return parseDefinitions(inputStream,
                                null,
                                null,
                                null,
                                null);
    }

    /**
     * Parses the BPMN XML contains in the provided {@link InputStream}
     *
     * @param inputStream
     *            the {@link InputStream} containing the BPMN XML.
     * @param consumer
     *            the {@link Consumer} instance that will consume the
     *            {@link FlowNodeTask} instances from the supplier.
     * @param supplier
     *            the {@link Supplier} instance to which {@link FlowNodeTask}
     *            instance in an executing workflow will be added.
     * @param suspendable
     *            the {@link Suspendable} instance used by this object to know
     *            whether it is active or not.
     * @param path
     *            the {@link Path} instance, if any, to the file containing the set
     *            of names of suspended {@link FlowNodeTask} instances.
     * @throws ClassNotFoundException
     *             then class requested by the BPMXML can not be found.
     * @throws JAXBException
     *             when there is an issue with parse the
     * @throws IOException
     *             the there is an issue with the {@link InputStream} provided.
     * @throws NoSuchElementException
     *             when the BPMN declares an element that does not exist.
     * @throws NoSuchMethodException
     *             when no external method can be mapped to one or more of the
     *             {@link Operation} instance being created.
     *
     * @return returns a map of names and {@link Process} instances.
     */
    public Map<String, ? extends ProcessDefinition> parseDefinitions(InputStream inputStream,
                                                                     Consumer consumer,
                                                                     Supplier<FlowNodeTask> supplier,
                                                                     Suspendable suspendable,
                                                                     Path path) throws ClassNotFoundException,
                                                                                JAXBException,
                                                                                IOException,
                                                                                NoSuchElementException,
                                                                                NoSuchMethodException {
        final JAXBContext jc = JAXBContext.newInstance("gov.lbl.nest.lazarus.xml.bind");
        final Unmarshaller u = jc.createUnmarshaller();
        @SuppressWarnings("unchecked")
        final JAXBElement<TDefinitions> unmarshalledXml = (JAXBElement<TDefinitions>) u.unmarshal(inputStream);
        inputStream.close();
        return parseDefinitions(unmarshalledXml.getValue(),
                                supplier,
                                consumer,
                                suspendable,
                                path);
    }

    /**
     * Parses a {@link TDefinitions} instance and extracts the collection of
     * {@link Process} instance.
     * 
     * @param definitions
     *            the {@link TDefinitions} instance from which to extract the
     *            {@link Process} instances.
     * @param supplier
     *            the {@link Supplier} instance to which {@link FlowNodeTask}
     *            instance in an executing workflow will be added.
     * @param consumer
     *            the {@link Consumer} instance that will consume the
     *            {@link FlowNodeTask} instances from the supplier.
     * @param suspenable
     *            the {@link AtomicBoolean} that is <code>true</code> when the
     *            {@link AtomicBoolean} that is <code>true</code> when the
     *            {@link Process} instances is active.
     * @param path
     *            the {@link Path} instance, if any, to the file containing the set
     *            of names of suspended {@link FlowNodeTask} instances.
     * 
     * @return the collection of {@link Process} instances created.
     *
     * @throws ClassNotFoundException
     *             when a requested {@link Class} can not be found.
     * @throws NoSuchMethodException
     *             when no external method can be mapped to one or more of the
     *             {@link Operation} instance being created.
     * @throws NoSuchElementException
     *             when the BPMN declares an element that does not exist.
     */
    private Map<String, ? extends ProcessDefinition> parseDefinitions(final TDefinitions definitions,
                                                                      Supplier<FlowNodeTask> supplier,
                                                                      Consumer consumer,
                                                                      Suspendable suspenable,
                                                                      Path path) throws ClassNotFoundException,
                                                                                 NoSuchElementException,
                                                                                 NoSuchMethodException {
        final List<JAXBElement<? extends TRootElement>> rootElements = definitions.getRootElement();
        final List<TItemDefinition> itemDefinitionDefinitions = new ArrayList<>();
        final List<TMessage> messageDefinitions = new ArrayList<>();
        final List<TInterface> interfaceDefinitions = new ArrayList<>();
        final List<TProcess> processDefinitions = new ArrayList<>();
        for (JAXBElement<? extends TRootElement> jaxbElement : rootElements) {
            final TRootElement rootElement = jaxbElement.getValue();
            if (rootElement instanceof TItemDefinition) {
                itemDefinitionDefinitions.add((TItemDefinition) rootElement);
            } else if (rootElement instanceof TMessage) {
                messageDefinitions.add((TMessage) rootElement);
            } else if (rootElement instanceof TInterface) {
                interfaceDefinitions.add((TInterface) rootElement);
            } else if (rootElement instanceof TProcess) {
                processDefinitions.add((TProcess) rootElement);
            } else {
                throw new NoSuchElementException("This application does not know how to handle a " + (rootElement.getClass()).getName()
                                                 + " element");
            }
        }

        final Map<String, ItemDefinition> itemDefinitions = new HashMap<>();
        for (TItemDefinition itemDefinition : itemDefinitionDefinitions) {
            final ItemDefinition created = createItemDefinition(itemDefinition);
            itemDefinitions.put(created.getIdentity(),
                                created);
        }

        final Map<String, Message> messages = new HashMap<>();
        for (TMessage message : messageDefinitions) {
            final Message created = createMessage(message,
                                                  itemDefinitions);
            messages.put(created.getIdentity(),
                         created);
        }

        final List<Interface> interfaces = new ArrayList<>();
        for (TInterface iface : interfaceDefinitions) {
            final Interface created = createInterface(iface,
                                                      messages);
            interfaces.add(created);
        }

        final Map<String, Operation> operations = new HashMap<>();
        for (Interface iface : interfaces) {
            Collection<? extends Operation> ops = iface.getOperations();
            for (Operation op : ops) {
                operations.put(op.getIdentity(),
                               op);
            }
        }

        final List<Process> processes = new ArrayList<>();
        for (TProcess process : processDefinitions) {
            final Process created = createProcess(process,
                                                  operations,
                                                  itemDefinitions,
                                                  consumer,
                                                  supplier,
                                                  suspenable,
                                                  path);
            processes.add(created);
        }

        // This is a cheat. Need to work out how to do this right.
        final Map<String, ProcessDefinition> results = new HashMap<>();
        for (Process process : processes) {
            results.put(process.getName(),
                        (ProcessDefinition) process);
        }
        return results;
    }

    /**
     * Returns the {@link TDefinitions} instance constructed from the provided
     * Processes.
     *
     * @param processes
     *            the {@link Process} instances from which to construct the
     *            {@link TDefinitions} instance.
     *
     * @return the {@link TDefinitions} instance constructed from the provided
     *         Processes.
     */
    public TDefinitions toDefinition(List<Process> processes) {
        ObjectFactory factory = new ObjectFactory();
        final TDefinitions result = factory.createTDefinitions();

        final List<JAXBElement<? extends TRootElement>> rootElements = result.getRootElement();

        final Set<ItemDefinition> itemDefinitions = new HashSet<>();
        for (Process process : processes) {
            itemDefinitions.addAll(process.getItemDefinitions());
        }
        for (ItemDefinition itemDefinition : itemDefinitions) {
            QName qName = new QName(itemDefinition.getIdentity());
            TItemDefinition created = toItemDefinition(itemDefinition);
            JAXBElement<? extends TRootElement> element = new JAXBElement<>(qName,
                                                                            TRootElement.class,
                                                                            created);
            rootElements.add(element);
        }

        final Set<Message> messages = new HashSet<>();
        for (Process process : processes) {
            messages.addAll(process.getMessages());
        }
        for (Message message : messages) {
            QName qName = new QName(message.getIdentity());
            TMessage created = toMessage(message);
            JAXBElement<? extends TRootElement> element = new JAXBElement<>(qName,
                                                                            TRootElement.class,
                                                                            created);
            rootElements.add(element);
        }

        final Set<Interface> interfaces = new HashSet<>();
        for (Process process : processes) {
            interfaces.addAll(process.getInterfaces());
        }
        for (Interface iface : interfaces) {
            QName qName = new QName(iface.getIdentity());
            TInterface created = toInterface(iface);
            JAXBElement<? extends TRootElement> element = new JAXBElement<>(qName,
                                                                            TRootElement.class,
                                                                            created);
            rootElements.add(element);
        }

        return null;
    }

    /**
     * Returns the {@link TInterface} instance constructed from the provided
     * {@link Interface} instance.
     *
     * @return the {@link TInterface} instance constructed from the provided
     *         {@link Interface} instance.
     */
    private TInterface toInterface(Interface iface) {
        ObjectFactory factory = new ObjectFactory();
        final TInterface result = factory.createTInterface();
        result.setId(iface.getIdentity());
        QName qname = new QName(JAVATYPE_NAMESPACE,
                                (iface.getImplementationRef()));
        result.setImplementationRef(qname);
        result.setName(iface.getName());
        final Collection<TOperation> ops = result.getOperation();
        for (Operation operation : iface.getOperations()) {
            ops.add(toOperation(operation));
        }
        return result;
    }

    /**
     * Returns the {@link TItemDefinition} instance constructed from the provided
     * {@link ItemDefinition} instance.
     *
     * @return the {@link TItemDefinition} instance constructed from the provided
     *         {@link ItemDefinition} instance.
     */
    private TItemDefinition toItemDefinition(ItemDefinition definition) {
        ObjectFactory factory = new ObjectFactory();
        final TItemDefinition result = factory.createTItemDefinition();
        result.setId(definition.getIdentity());
        result.setIsCollection(definition.isCollection());
        QName qname = new QName(JAVATYPE_NAMESPACE,
                                (definition.getClass()).getName());
        result.setStructureRef(qname);
        return result;
    }

    /**
     * Returns the {@link TMessage} instance constructed from the provided
     * {@link Message} instance.
     *
     * @return the {@link TMessage} instance constructed from the provided
     *         {@link Message} instance.
     */
    private TMessage toMessage(Message message) {
        ObjectFactory factory = new ObjectFactory();
        final TMessage result = factory.createTMessage();
        result.setId(message.getIdentity());
        QName qname = new QName((message.getItemRef()).getIdentity());
        result.setItemRef(qname);
        result.setName(message.getName());
        return result;
    }

    /**
     * Returns the {@link TOperation} instance constructed from the provided
     * {@link Operation} instance.
     *
     * @return the {@link TOperation} instance constructed from the provided
     *         {@link Operation} instance.
     */
    private TOperation toOperation(Operation operation) {
        ObjectFactory factory = new ObjectFactory();
        final TOperation result = factory.createTOperation();
        result.setId(operation.getIdentity());
        QName qname = new QName((((operation.getInMessageRef()).getItemRef()).getStructure()).getName());
        result.setInMessageRef(qname);
        QName qname2 = new QName((((operation.getOutMessageRef()).getItemRef()).getStructure()).getName());
        result.setOutMessageRef(qname2);
        result.setName(operation.getName());
        return result;
    }

}
