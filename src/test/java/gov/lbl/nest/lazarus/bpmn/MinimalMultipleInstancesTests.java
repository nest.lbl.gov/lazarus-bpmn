package gov.lbl.nest.lazarus.bpmn;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.NoSuchElementException;

import jakarta.xml.bind.JAXBException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import gov.lbl.nest.common.testing.UnitTestUtility;
import gov.lbl.nest.lazarus.execution.ProcessDefinition;
import gov.lbl.nest.lazarus.structure.Operation;

/**
 * This is the set of test to be passed by the {@link XMLDefinitions} class.
 *
 * @author patton
 */
@DisplayName("Minimal Multiple Instances Tests")
public class MinimalMultipleInstancesTests {

    /**
     * The BPMN resource hold the minimal service task diagram.
     */
    private final String MINIMAL_MULTIPLE_INSTANCE_BPMN = "MinimalMultipleInstances.bpmn";

    /**
     * Test that a minimal service task can be read and executed from an XML
     * document.
     *
     * @throws ClassNotFoundException
     *             then class requested by the BPMXML can not be found.
     * @throws JAXBException
     *             when there is an issue with parse the
     * @throws IOException
     *             the there is an issue with the {@link InputStream} provided.
     * @throws NoSuchElementException
     *             when the BPMN declares an element that does not exist.
     * @throws NoSuchMethodException
     *             when no external method can be mapped to one or more of the
     *             {@link Operation} instance being created.
     */
    @Test
    @DisplayName("Execution from XML")
    void executionFromXML() throws ClassNotFoundException,
                            NoSuchElementException,
                            JAXBException,
                            IOException,
                            NoSuchMethodException {
        final XMLDefinitions testObject = new XMLDefinitions();

        final InputStream is = getClass().getResourceAsStream(MINIMAL_MULTIPLE_INSTANCE_BPMN);
        final Map<String, ? extends ProcessDefinition> processes = testObject.parseDefinitions(is);
        assertEquals(1,
                     processes.size());
    }

    /**
     * Sets up the environment before each test.
     */
    @BeforeEach
    void setUp() {
        UnitTestUtility.deleteTree(new File("lazarus"));
    }

    /**
     * Tears down the environment after each test.
     */
    @AfterEach
    void tearDown() {
        UnitTestUtility.deleteTree(new File("lazarus"));
    }
}
