# Welcome to `lazarus-bpmn` #

## Overview ##

[lazarus-bpmn](http://nest.lbl.gov.gitlab.io/lazarus-bpmn/) contains classes that can build a [lazarus](http://nest.lbl.gov.gitlab.io/lazarus/) process from a BPMN XML diagram.

To use it the main class, `XMLDefinitions`, need to be instantiated with the appropriate definitions to produce functioning process instances. For example

    final XMLDefinitions xmlDefinitions = new XMLDefinitions(new FnNamespaceContextImpl(null),
                                                             new FnXPathFunctionResolverImpl(null),
                                                             null,
                                                             false);
    final Map<String, ? extends ProcessDefinition> processes = xmlDefinitions.parseDefinitions(inputStream,
                                                                                               consumer,
                                                                                                       supplierToUse,
                                                                                                       new SuspendableImpl(name,
                                                                                                                           suspended),
                                                                                                       pathToUse);

Where `FnNamespaceContextImpl` and `FnXPathFunctionResolverImpl` are implementations of the [`NamespaceContext``](https://docs.oracle.com/en/java/javase/11/docs/api/java.xml/javax/xml/namespace/NamespaceContext.html) and [`XPathFunctionResolver`](https://docs.oracle.com/en/java/javase/11/docs/api/java.xml/javax/xml/xpath/XPathFunctionResolver.html) interfaces respectively. While the `inputStream` is an `InputStream` instance through which the BPMN XML document can be read.
